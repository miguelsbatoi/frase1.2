'use strict'
function letras(cadena) {
return cadena.length
}

function palabras(cadena) {
return cadena.split(" ").length
}

function maysc(cadena) {
return cadena.toLocaleUpperCase()
}

function titulo(cadena) {
	var palabras = cadena.split(" ")
	for (let i = 0; i < palabras.length; i++) {
    palabras[i] = palabras[i][0].toUpperCase() + palabras[i].substr(1);
}
return palabras.join(" ")
}

function letrasReves(cadena) {
return cadena.split("").reverse().join("")
}

function palabrasReves(cadena) {
return cadena.split(" ").reverse().join(" ")
}

function palindromo(cadena) {
var capicua = cadMinus.split("").reverse().join("")
if (capicua == cadena) {
	return true;
}
return false;
}

module.exports = {
	letras,
	palabras,
	maysc,
	titulo,
	letrasReves,
	palabrasReves,
	palindromo
}
